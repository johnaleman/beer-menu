(function() {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), '/home');
    }

    function getStates() {
        return [
            {
                state: 'app.beer',
                config: {
                    abstract: true,
                    url: '/beer',
                    template: '<ui-view/>'
                }
            },
            {
                state: 'app.beer.list',
                config: {
                    url: '',
                    templateUrl: 'js/main/beer/list/list.html',
                    controller: 'BeerListCtrl',
                    controllerAs: 'vm',
                    title: 'beer list',
                    params: {
                        param: 'test param'
                    }
                }
            },
            {
                state: 'app.beer.list.item',
                config: {
                    url: '/{id}',
                    templateUrl: 'js/main/beer/item/item.html',
                    controller: 'BeerItemCtrl',
                    controllerAs: 'vm',
                    title: 'beer item',
                    params: {
                        param: 'test param'
                    }
                }
            }
        ];
    }
})();