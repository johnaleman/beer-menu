(function() {
    'use strict';

    angular
        .module('app')
        .controller('BeerListCtrl', BeerListCtrl);

    BeerListCtrl.$inject = ['$log'];

    /* @ngInject */
    function BeerListCtrl($log) {
        var vm = this;
        vm.beerList = [];

        init();

        function init() {
            $log.log('[BeerListCtrl] - activate');

            // set beer list
            vm.beerList = getBeerList().data;
            $log.log('vm.beerList', vm.beerList);
        }

        function getBeerList() {
            var data = {"message":"Request Successful","data":[{"id":"gqUc4A","name":"Tornado Ale","nameDisplay":"Tornado Ale","glasswareId":5,"availableId":5,"styleId":25,"isOrganic":"N","status":"verified","statusDisplay":"Verified","createDate":"2012-01-03 02:44:23","updateDate":"2015-01-08 17:33:29","glass":{"id":5,"name":"Pint","createDate":"2012-01-03 02:41:33"},"available":{"id":5,"name":"Spring","description":"Available during the spring months."},"style":{"id":25,"categoryId":3,"category":{"id":3,"name":"North American Origin Ales","createDate":"2012-03-21 20:06:45"},"name":"American-Style Pale Ale","shortName":"American Pale","description":"American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.","ibuMin":"30","ibuMax":"42","abvMin":"4.5","abvMax":"5.6","srmMin":"6","srmMax":"14","ogMin":"1.044","fgMin":"1.008","fgMax":"1.014","createDate":"2012-03-21 20:06:45","updateDate":"2015-04-07 15:25:18"}},{"id":"6lJ7FY","name":"Pumpkin Ale","nameDisplay":"Pumpkin Ale","description":"Unlike most pumpkin beers, we make this with real pumpkin! We add pumpkin throughout the boil giving it a rich, underlying sweetness and flavor. Then finish with our blend of seasonal spices to make this beer the perfect blend of a beer and pumpkin. This brew is perfect for those delightful autumn evenings!","abv":"6.1","ibu":"30","styleId":121,"isOrganic":"Y","status":"verified","statusDisplay":"Verified","createDate":"2015-06-22 22:45:12","updateDate":"2015-06-22 22:45:12","style":{"id":121,"categoryId":11,"category":{"id":11,"name":"Hybrid/mixed Beer","createDate":"2012-03-21 20:06:46"},"name":"Pumpkin Beer","shortName":"Pumpkin Beer","description":"Pumpkin beers are any beers using pumpkins (Cucurbito pepo) as an adjunct in either mash, kettle, primary or secondary fermentation, providing obvious (ranging from subtle to intense), yet harmonious, qualities. Pumpkin qualities should not be overpowered by hop character. These may or may not be spiced or flavored with other things. A statement by the brewer explaining the nature of the beer is essential for fair assessment in competitions. If this beer is a classic style with pumpkin, the brewer should also specify the classic style.","ibuMin":"5","ibuMax":"70","abvMin":"2.5","abvMax":"12","srmMin":"5","srmMax":"50","ogMin":"1.03","fgMin":"1.006","fgMax":"1.03","createDate":"2012-03-21 20:06:46","updateDate":"2015-04-07 15:44:28"}},{"id":"CUZJZV","name":"Pale Ale","nameDisplay":"Pale Ale","styleId":25,"isOrganic":"N","status":"verified","statusDisplay":"Verified","createDate":"2012-01-03 02:43:54","updateDate":"2012-03-22 13:04:48","style":{"id":25,"categoryId":3,"category":{"id":3,"name":"North American Origin Ales","createDate":"2012-03-21 20:06:45"},"name":"American-Style Pale Ale","shortName":"American Pale","description":"American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.","ibuMin":"30","ibuMax":"42","abvMin":"4.5","abvMax":"5.6","srmMin":"6","srmMax":"14","ogMin":"1.044","fgMin":"1.008","fgMax":"1.014","createDate":"2012-03-21 20:06:45","updateDate":"2015-04-07 15:25:18"}}],"status":"success"};

            return data;
        }
    }
})();